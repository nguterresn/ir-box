
#define F_CPU 1000000UL

#include "avr_compiler.h"
#include <util/delay.h>
#include <avr/io.h>
#include <string.h>

#define FOSC 1000000 // Clock Speed
#define BAUD 4800
#define MYUBRR FOSC/16/BAUD-1

uint8_t k;
uint8_t flag_onoff = 0;
uint8_t flag_updown = 0;

void USART_Init(uint32_t ubrr)
{
	/*Set baud rate */
	UBRR0H = (uint8_t)(ubrr>>8);
	UBRR0L = (uint8_t)ubrr;
	/*Enable receiver and transmitter */
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	/* Set frame format: 8data, 2stop bit */
	UCSR0C = (1<<USBS0)|(3<<UCSZ00);
}

void USART_Transmit(uint8_t *data )
{
	k=0;
	while( data[k] != '\0' )
	{
		/* Wait for empty transmit buffer */
		while ( !( UCSR0A & (1<<UDRE0)) );
		/* Put data into buffer, sends the data */
		UDR0 = data[k];
		k++;
	}
}

void init()
{
	ADCSRA=(1<<ADEN)|(1<<ADPS0)|(1<<ADPS1); //Presclare de 8. 1*10^6/8=125khz

	ADMUX=(1<<REFS0)|(1<<ADLAR);// AVCC ligado com VCC //

	// ADC NOISE REDUCTION
	SMCR |= (1<<SM0);
}

uint16_t ReadADC(uint8_t ch)
{
	//Select ADC Channel ch must be 0-7
	ch=ch & 0b00000111;
	//first clear the old channel
	ADMUX &= 0b11111000;

	ADMUX|=ch;

	//Start Single conversion
	ADCSRA|=(1<<ADSC);

	 // wait for conversion to complete
	 // ADSC becomes ’0? again
	 // till then, run loop continuously
	 while(ADCSRA & (1<<ADSC));

	uint8_t low = ADCL;
	uint16_t tenbitADC = (ADCH << 2)|(low >> 6);
	return tenbitADC;

}

int main(void)
{

	USART_Init(MYUBRR);
	init();

  DDRD |= (1<<PD2); //blink led
	PORTD &= ~(1<<PD2);

	uint8_t adc_value[20];

  while(1)
  {
		if (ReadADC(0) <= 800) //LIGA
		{
			PORTD |= (1<<PD2);
			itoa(ReadADC(0), adc_value, 10);
			USART_Transmit(adc_value);
			flag_onoff++;
			_delay_ms(400);
		}

		if (flag_onoff == 2) {
			flag_onoff = 0;
			PORTD &= ~(1<<PD2); //Desliga led
		}

  }
}
